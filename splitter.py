#!/usr/bin/env python

# Based on:
#  https://antoniovazquezblanco.github.io/blog/New-InsydeH2O-BIOS-update-format.html
#  https://ristovski.github.io/posts/inside-insydeh2o/

import os
import argparse

# this list matches the order found in the isflash.bin file
magic = [ b'MZ', b'$_IFLASH_DRV_IMG', b'$_IFLASH_BIOSIMG', b'$_IFLASH_INI_IMG', b'$_IFLASH_EC_IMG_' ]
len_tag = 16
len_offset = 8

parser = argparse.ArgumentParser(description="Split Insyde H2O firmware files.")
parser.add_argument("-f", "--file", dest="filename", help="File to be processed.", metavar="file")
args = parser.parse_args()
prefix = os.path.dirname(args.filename) + './' +os.path.splitext(os.path.basename(args.filename))[0] + '/'

if not os.path.exists(prefix):
    os.makedirs(prefix)

with open(args.filename, 'rb') as fd:
    content = fd.read()

offsets = []
for section in magic:
    offsets.append(content.find(section))
offsets.append(len(content))

offsets[0] -= (len_tag + len_offset)  # no tag or length for MZ section

for n in range(len(magic)):
    binname = magic[n][1:] if magic[n][0] is '$' else magic[n]
    start = offsets[n] + len_tag + len_offset
    end = offsets[n + 1] & ~0xf
    print('File {} <= content[{:#08x}:{:#08x}], {:#05x} bytes'.format(binname, start, end, end - start))
    with open(prefix + binname, 'wb') as img:
        img.write(content[start:end])
